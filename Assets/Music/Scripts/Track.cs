﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class Track {
    public AudioClip Song;
    public AnimationCurve QualityByTime;
    //todo fix bug with calculating quality
    //todo press space for beat for debugging

    //Time window "radius" in ms
    public float GestureTimeWindow;
    public List<Beat> Beats = new List<Beat>();
    public List<SFXBeat> SoundEffects = new List<SFXBeat>();

    public bool OnGestureReceived(Gesture.Type gestureName, float timeReceived)
    {
        bool beatFound = false;

        foreach (Beat beat in Beats)
        {
            if (beatFound)
                break;

            if ((!beat.AcceptedGestures.Contains(gestureName) && gestureName != Gesture.Type.Any) || beat.Status == BeatStatus.Success)
            {
                continue;
            }

            //Time the gesture was expected
            float expectedTime  = 0;
            if(gestureName != Gesture.Type.Any)
            {
                expectedTime = beat.Time + TrackManager.KinectDelay;
            }
            else
            {
                expectedTime = beat.Time;
            }

            //difference between gesture and expected gesture time
            float deviation = timeReceived - expectedTime;

            //Debug.Log("Expected Time: " + expectedTime);
            //Debug.Log("Deviation: " + deviation);

            //is the deviation acceptable
            if (Mathf.Abs(deviation) <= GestureTimeWindow)
            {
                float quality = QualityByTime.Evaluate((deviation / GestureTimeWindow) / 2 + 0.5f);

                beat.Quality = quality;

                //Debug.Log("Quality: " + quality);

                beatFound = true;
            }
        }

        /*if (!beatFound)
        {
            Debug.Log("No Beat Found");
        }*/

        return beatFound;
    }

    public Beat GetLastBeat(float timeInMs)
    {
        Beat result = null;
        //beats are ordered by time
        foreach (Beat b in Beats)
        {
            //Beat is before the given time
            if (timeInMs >= b.Time)
            {
                result = b;
            }
            //Beat is after given time
            else
            {
                //there was no beat before this one
                if (result == null)
                {
                    return null;
                }
                else
                {
                    return result;
                }
            }
        }

        return null;
    }

    public Beat GetLastBeat(float timeInMs, bool withTimeWindow)
    {

        if (withTimeWindow)
        {
            Beat result = null;
            //beats are ordered by time
            foreach (Beat b in Beats)
            {
                //Beat is before the given time
                if (timeInMs >= b.Time + GestureTimeWindow)
                {
                    result = b;
                }
                //Beat is after given time
                else
                {
                    //there was no beat before this one
                    if (result == null)
                    {
                        return null;
                    }
                    else
                    {
                        return result;
                    }
                }
            }

            return null;
        }
        else
        {
            return GetLastBeat(timeInMs);
        }
    }

    public Beat GetNextBeat(float timeInMs)
    {
        //beats are ordered by time
        foreach (Beat b in Beats)
        {
            if (timeInMs < b.Time)
            {
                return b;
            }
        }

        return null;
    }

    public Beat GetNextBeat(float timeInMs, bool withTimeWindow)
    {

        if (withTimeWindow)
        {
            //beats are ordered by time
            foreach (Beat b in Beats)
            {
                if (timeInMs < b.Time + GestureTimeWindow)
                {
                    return b;
                }
            } 
            return null;
        }
        else
        {
            return GetNextBeat(timeInMs);
        }
    }
}
