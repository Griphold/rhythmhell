﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

[RequireComponent(typeof(AudioSource))]
public class TrackManager : MonoBehaviour
{
    public static TrackManager Instance;
    public static float KinectDelay = 100;//Kinect Gestures have a delay of about 100 ms
    public static float Volume = 1;

    public void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(this);
        }

        foreach (Track t in TrackList)
        {
            t.QualityByTime = QualityByTime;
        }

        SetVolume(Volume);

        StartCoroutine(SetTrack(TrackList[startingTrack], StartDelayInSeconds, startOffsetInSeconds));
    }
    public float StartDelayInSeconds;

    public AnimationCurve QualityByTime;
    public int startingTrack;//temporary
    public float startOffsetInSeconds;
    public List<Track> TrackList = new List<Track>();
    public event EventHandler<BeatEventArgs> BeatOverEvent;

    public AudioSource audioSource;
    public AudioSource SFXAudioSource;
    private Track activeTrack;
    private List<Beat> processedBeats = new List<Beat>();
    private bool started = false;

    // Use this for initialization
    void Start()
    {
        //audioSource.time = 100;
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            OnGestureReceived(Gesture.Type.Any);
        }

        //Set Score at the end of the track
        if (IsActiveTrackOver())
        {
            GameObject g = GameObject.FindGameObjectWithTag("Score");
            if (g)
            {
                g.GetComponent<ScoreHandler>().SetScore(GetAccuracy());
            }
        }

        //check if a beat was missed
        Beat b = GetLastBeat(true);
        if (b != null && !processedBeats.Contains(b))
        {
            processedBeats.Add(b);
            if (b.Status == BeatStatus.Failed && BeatOverEvent != null)
            {
                BeatOverEvent(this, new BeatEventArgs(b));
            }
        }
        List<SFXBeat> sfxList = activeTrack.SoundEffects;

        for (int i = activeTrack.SoundEffects.Count - 1; i >= 0; i--)
        {
            SFXBeat sfx = activeTrack.SoundEffects[i];
            if (sfx.Time <= GetActiveTrackTimeInMs() && sfx.SFX != null)
            {
                SFXAudioSource.PlayOneShot(sfx.SFX);
                sfxList.RemoveAt(i);
            }
        }
    }

    void OnDestroy()
    {
        TrackManager.Instance = null;
    }

    public void OnGestureReceived(Gesture.Type gestureName)
    {
        bool beatFound = activeTrack.OnGestureReceived(gestureName, GetActiveTrackTimeInMs());

        if(!beatFound && BeatOverEvent != null)
        {
            BeatOverEvent(this, new BeatEventArgs(null));
        }


    }

    private IEnumerator SetTrack(Track track, float startDelay, float startTime)
    {
        activeTrack = track;

        yield return new WaitForSeconds(startDelay);

        audioSource.clip = track.Song;
        audioSource.Play();

        audioSource.time = startTime;

        started = true;
    }

    public void SetVolume(float volume)
    {
        if (audioSource != null)
        {
            audioSource.volume = Mathf.Clamp01(volume);
            Volume = volume;
        }
    }

    public Track GetActiveTrack()
    {
        return activeTrack;
    }

    public Beat GetLastBeat()
    {
        return activeTrack.GetLastBeat(GetActiveTrackTimeInMs());
    }

    public Beat GetLastBeat(bool withTimeWindow)
    {
        return activeTrack.GetLastBeat(GetActiveTrackTimeInMs(), withTimeWindow);
    }

    public Beat GetNextBeat()
    {
        return activeTrack.GetNextBeat(GetActiveTrackTimeInMs());
    }

    public Beat GetNextBeat(bool withTimeWindow)
    {
        return activeTrack.GetNextBeat(GetActiveTrackTimeInMs(), withTimeWindow);
    }

    public Beat GetNextBeat(float trackTimeInMs)
    {
        return activeTrack.GetNextBeat(trackTimeInMs);
    }

    public bool HasStarted()
    {
        return started;
    }

    void OnDrawGizmos()
    {
        //dont draw gizmo if there is no active track
        if (activeTrack == null)
        {
            return;
        };

        //Draws beat cube with gesture window
        float timeScale = Screen.width * 0.1f; //1 second = 10% of screen width
        float gestureWindow = timeScale * activeTrack.GestureTimeWindow * 0.001f;

        Vector3 beatLineLeftTop = new Vector3(Screen.width / 2 - gestureWindow, Screen.height * 0.17f, Camera.main.nearClipPlane + 0.1f);
        Vector3 beatLineRightBottom = new Vector3(Screen.width / 2 + gestureWindow, Screen.height * 0.08f, Camera.main.nearClipPlane + 0.1f);


        Vector3 beatLineLeftBottom = Camera.main.ScreenToWorldPoint(new Vector3(beatLineLeftTop.x, beatLineRightBottom.y, Camera.main.nearClipPlane + 0.1f));
        Vector3 beatLineRightTop = Camera.main.ScreenToWorldPoint(new Vector3(beatLineRightBottom.x, beatLineLeftTop.y, Camera.main.nearClipPlane + 0.1f));
        Vector3 beatLineCenterTop = Camera.main.ScreenToWorldPoint(new Vector3((beatLineLeftTop.x + beatLineRightBottom.x) / 2, beatLineLeftTop.y, Camera.main.nearClipPlane + 0.1f));
        Vector3 beatLineCenterBottom = Camera.main.ScreenToWorldPoint(new Vector3((beatLineLeftTop.x + beatLineRightBottom.x) / 2, beatLineRightBottom.y, Camera.main.nearClipPlane + 0.1f));
        beatLineLeftTop = Camera.main.ScreenToWorldPoint(beatLineLeftTop);
        beatLineRightBottom = Camera.main.ScreenToWorldPoint(beatLineRightBottom);

        Gizmos.color = Color.yellow;
        Gizmos.DrawLine(beatLineLeftTop, beatLineLeftBottom);
        Gizmos.DrawLine(beatLineCenterTop, beatLineCenterBottom);
        Gizmos.DrawLine(beatLineRightTop, beatLineRightBottom);

        //draw "perfect" beat line
        //Gizmos.DrawLine(beatLineCenter + new Vector3(0, scale.y / 2, 0), beatLineCenter - new Vector3(0, scale.y / 2, 0));

        //draw lines for all beats
        foreach (Beat beat in activeTrack.Beats)
        {
            //Draws beat
            Vector3 beatStart = new Vector3(Screen.width / 2, Screen.height * 0.15f, Camera.main.nearClipPlane + 0.1f);
            Vector3 beatEnd = new Vector3(Screen.width / 2, Screen.height * 0.1f, Camera.main.nearClipPlane + 0.1f);

            beatStart.x += (beat.Time * 0.001f - audioSource.time) * timeScale;
            beatEnd.x += (beat.Time * 0.001f - audioSource.time) * timeScale;

            beatStart = Camera.main.ScreenToWorldPoint(beatStart);
            beatEnd = Camera.main.ScreenToWorldPoint(beatEnd);

            Gizmos.color = beat.Status == BeatStatus.Failed ? Color.red : Color.green;
            Gizmos.DrawLine(beatStart, beatEnd);
        }
    }

    public float GetActiveTrackTimeInMs()
    {
        return audioSource.time * 1000;
    }

    public float GetActiveTrackLengthInMs()
    {
        return audioSource.clip.length * 1000;
    }

    public void SetPaused(bool val)
    {
        if (val)
        {
            audioSource.Pause();
        }
        else
        {
            audioSource.UnPause();
        }
    }

    public bool IsActiveTrackOver()
    {
        return audioSource.clip == null || GetActiveTrackTimeInMs() >= GetActiveTrackLengthInMs() - 500;
    }

    private float GetAverageQuality()
    {
        List<Beat> beatles = activeTrack.Beats;

        float sum = 0;
        foreach (Beat b in beatles)
        {
            sum += 1 - Mathf.Abs(b.Quality);
        }

        float average = sum / beatles.Count;

        return average;
    }

    private float GetAccuracy()
    {
        List<Beat> beatles = activeTrack.Beats;

        float sum = 0;
        foreach (Beat b in beatles)
        {
            sum += b.Status == BeatStatus.Success ? 1 : 0;
        }

        float accuracy = sum / beatles.Count;

        return accuracy;
    }
}
