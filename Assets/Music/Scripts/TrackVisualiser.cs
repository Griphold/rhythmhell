﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class TrackVisualiser : MonoBehaviour {
    public GameObject UIBeatImage;
    public GameObject UIFlapImage;
    public GameObject UIVegetableImage;
    public RectTransform GestureWindowImage;
    public RectTransform BeatCanvas;

    private List<Beat> beatList;
    private List<Transform> beatTransform = new List<Transform>();
    private List<Image> beatImages = new List<Image>();
    private float timeScale;
	// Use this for initialization
	void Start () {
        
        //10% of screen width = 1 second, 0.001 converts from ms to s
        timeScale = Screen.width * 0.3f * 0.001f;

        beatList = TrackManager.Instance.GetActiveTrack().Beats;

        foreach (Beat beat in beatList)
        {
            GameObject g = null;

            switch (beat.type)
            {
                case Beat.Type.Basic:
                    g = (GameObject) Instantiate(UIBeatImage);
                    break;

                case Beat.Type.Flap:
                    g = (GameObject) Instantiate(UIFlapImage);
                    break;

                case Beat.Type.Vegetable:
                    g = (GameObject) Instantiate(UIVegetableImage);
                    break;
            }

            g.transform.SetParent(BeatCanvas, false);
            g.transform.localPosition = new Vector3(beat.Time * timeScale, -420, 0);
            beatTransform.Add(g.transform);
            beatImages.Add(g.GetComponent<Image>());
        }

        float timeWindow = TrackManager.Instance.GetActiveTrack().GestureTimeWindow;

        GestureWindowImage.sizeDelta = new Vector2(timeWindow  * timeScale * 2, GestureWindowImage.sizeDelta.y);
	}
	
	// Update is called once per frame
	void Update () 
    {
        float activeTrackTime = TrackManager.Instance.GetActiveTrackTimeInMs();
        float timeWindow = TrackManager.Instance.GetActiveTrack().GestureTimeWindow;
        for (int i = 0; i < beatTransform.Count; i++)
        {
            Transform t = beatTransform[i];
            Beat b = beatList[i];
            float time = b.Time - activeTrackTime;

            if (b.Status == BeatStatus.Success)
            {
                beatImages[i].color = Color.green;
            }
            else
            {
                if(time >= -timeWindow)
                {
                    beatImages[i].color = Color.white;
                }
                else
                {
                    beatImages[i].color = Color.red;
                }
                
            }

            t.localPosition = new Vector3(time * timeScale, -420, 0);
        }
	}
}
