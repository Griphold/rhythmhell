﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class SFXBeat : Beat
{

    public AudioClip SFX;
}
