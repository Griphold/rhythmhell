﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class Beat
{
    //Expected time of this beat in ms from start
    public float Time;
    public List<Gesture.Type> AcceptedGestures = new List<Gesture.Type>();
    public Type type = Beat.Type.Basic;

    //Quality of timing of the gesture
    //Ranges between -1 (in time but early), 0 (perfect timing) and 1 (in time but late)  
    private float quality;
    private BeatStatus status = BeatStatus.Failed;

    [SerializeField]
    public float Quality
    {
        get
        {
            return quality;
        }
        set
        {
            quality = value;

            if (Mathf.Abs(quality) > 1)
            {
                status = BeatStatus.Failed;
            }
            else
            {
                status = BeatStatus.Success;
            }
        }
    }

    public BeatStatus Status
    {
        get
        {
            return status;
        }
        set
        {
            status = value;
        }
    }

    public Beat Copy()
    {
        Beat b = new Beat();
        b.Time = Time;
        b.AcceptedGestures = AcceptedGestures;
        b.Quality = quality;
        b.type = type;
        b.status = status;
        return b;
    }

    public bool Equals (Beat otherBeat)
    {
        if (otherBeat == null)
            return false;

        return Time == otherBeat.Time;
    }

    public enum Type
    {
        Basic, Vegetable, Flap
    }
}

public enum BeatStatus
{
    //Failed: no gesture, wrong gesture, gesture not in time
    //Success: :D
    Failed, Success
}
