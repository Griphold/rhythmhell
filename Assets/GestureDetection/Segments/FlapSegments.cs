﻿using UnityEngine;
using Windows.Kinect;

/// <summary>
/// The first part of the Flap gesture
/// </summary>
public class FlapSegment1 : IRelativeGestureSegment
{
    /// <summary>
    /// Checks the gesture.
    /// </summary>
    /// <param name="skeleton">The skeleton.</param>
    /// <returns>GesturePartResult based on if the gesture part has been completed</returns>
    public GesturePartResult CheckGesture(BasicAvatarModel skeleton)
    {
        //Rotate Wings
        Vector3 leftShoulder = skeleton.getRawWorldPosition(JointType.ShoulderLeft);
        Vector3 rightShoulder = skeleton.getRawWorldPosition(JointType.ShoulderRight);
        Vector3 leftElbow = skeleton.getRawWorldPosition(JointType.ElbowLeft);
        Vector3 rightElbow = skeleton.getRawWorldPosition(JointType.ElbowRight);

        Vector2 lShoulderToElbow = new Vector2(leftElbow.x - leftShoulder.x, leftElbow.y - leftShoulder.y);
        Vector2 rShoulderToElbow = new Vector2(rightElbow.x - rightShoulder.x, rightElbow.y - rightShoulder.y);

        float angleLeft = Vector2.Angle(Vector2.down, lShoulderToElbow);
        float angleRight = Vector2.Angle(Vector2.down, rShoulderToElbow);

        if (angleLeft > 85 && angleRight > 85)
        {
            //Debug.Log("Flap 1 Success");
            return GesturePartResult.Succeed;
        }
        //Debug.Log("Flap 1 Failed");
        return GesturePartResult.Fail;
    }

}

/// <summary>
/// The second part of the Flap gesture
/// </summary>
public class FlapSegment2 : IRelativeGestureSegment
{
    /// <summary>
    /// Checks the gesture.
    /// </summary>
    /// <param name="skeleton">The skeleton.</param>
    /// <returns>GesturePartResult based on if the gesture part has been completed</returns>
    public GesturePartResult CheckGesture(BasicAvatarModel skeleton)
    {
        //Rotate Wings
        Vector3 leftShoulder = skeleton.getRawWorldPosition(JointType.ShoulderLeft);
        Vector3 rightShoulder = skeleton.getRawWorldPosition(JointType.ShoulderRight);
        Vector3 leftElbow = skeleton.getRawWorldPosition(JointType.ElbowLeft);
        Vector3 rightElbow = skeleton.getRawWorldPosition(JointType.ElbowRight);

        Vector2 lShoulderToElbow = new Vector2(leftElbow.x - leftShoulder.x, leftElbow.y - leftShoulder.y);
        Vector2 rShoulderToElbow = new Vector2(rightElbow.x - rightShoulder.x, rightElbow.y - rightShoulder.y);

        float angleLeft = Vector2.Angle(Vector2.down, lShoulderToElbow);
        float angleRight = Vector2.Angle(Vector2.down, rShoulderToElbow);

        if (angleLeft < 45 && angleRight < 45)
        {
            //Debug.Log("Flap 2 Success");
            return GesturePartResult.Succeed;
        }

        return GesturePartResult.Pausing;

    }
}
