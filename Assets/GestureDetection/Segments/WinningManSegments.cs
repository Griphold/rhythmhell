﻿using UnityEngine;
using Windows.Kinect;

/// <summary>
/// The first part of the winning man gesture
/// </summary>
public class WinningManSegment1 : IRelativeGestureSegment
{
    /// <summary>
    /// Checks the gesture.
    /// </summary>
    /// <param name="skeleton">The skeleton.</param>
    /// <returns>GesturePartResult based on if the gesture part has been completed</returns>
    public GesturePartResult CheckGesture(BasicAvatarModel skeleton)
    {
        Vector3 handRight = skeleton.getRawWorldPosition(JointType.HandRight);
        Vector3 handLeft = skeleton.getRawWorldPosition(JointType.HandLeft);
        Vector3 shoulderRight = skeleton.getRawWorldPosition(JointType.ShoulderRight);
        Vector3 shoulderLeft = skeleton.getRawWorldPosition(JointType.ShoulderLeft);
        Vector3 elbowRight = skeleton.getRawWorldPosition(JointType.ElbowRight);
        Vector3 elbowLeft = skeleton.getRawWorldPosition(JointType.ElbowLeft);
        Vector3 hipCenter = skeleton.getRawWorldPosition(JointType.SpineBase);
        Vector3 head = skeleton.getRawWorldPosition(JointType.Head);

        //Right Hand on the right side and left Hand left
        if (handRight.x > hipCenter.x && handLeft.x < hipCenter.x)
        {
            //Debug.Log("Start Winning Man 1");
            //Right Hand above Head and right of elbow
            if (handRight.y > head.y && handRight.x > elbowRight.x)
            {
                //Left Hand under shoulder and above hipCenter and right of elbow
                if (handLeft.y < shoulderLeft.y && handLeft.y > hipCenter.y && handLeft.x > elbowLeft.x)
                {
                    //Debug.Log("Winning Man 1 Succeed");
                    return GesturePartResult.Succeed;
                }
                return GesturePartResult.Pausing;
            }
            return GesturePartResult.Pausing;
        }
        return GesturePartResult.Fail;
    }
}

/// <summary>
/// The second part of the strong arm gesture
/// </summary>
public class WinningManSegment2 : IRelativeGestureSegment
{
    /// <summary>
    /// Checks the gesture.
    /// </summary>
    /// <param name="skeleton">The skeleton.</param>
    /// <returns>GesturePartResult based on if the gesture part has been completed</returns>
    public GesturePartResult CheckGesture(BasicAvatarModel skeleton)
    {
        Vector3 handRight = skeleton.getRawWorldPosition(JointType.HandRight);
        Vector3 handLeft = skeleton.getRawWorldPosition(JointType.HandLeft);
        Vector3 shoulderRight = skeleton.getRawWorldPosition(JointType.ShoulderRight);
        Vector3 shoulderLeft = skeleton.getRawWorldPosition(JointType.ShoulderLeft);
        Vector3 elbowRight = skeleton.getRawWorldPosition(JointType.ElbowRight);
        Vector3 elbowLeft = skeleton.getRawWorldPosition(JointType.ElbowLeft);
        Vector3 hipCenter = skeleton.getRawWorldPosition(JointType.SpineBase);
        Vector3 head = skeleton.getRawWorldPosition(JointType.Head);

        //Right Hand on the right side and left Hand left
        if (handRight.x > hipCenter.x && handLeft.x < hipCenter.x)
        {
            //Debug.Log("Start Winning Man 2");
            //Left Hand above Head and left of elbow
            if (handLeft.y > head.y && handLeft.x < elbowLeft.x)
            {
                //Right Hand under shoulder and above hipCenter and left of elbow
                if (handRight.y > hipCenter.y && handRight.y < shoulderRight.y && handRight.x < elbowRight.x)
                {
                    //Debug.Log("Winning Man 2 Succeed");
                    return GesturePartResult.Succeed;
                }
                return GesturePartResult.Pausing;

            }
            return GesturePartResult.Pausing;
        }
        //Debug.Log("2 Failed");
        return GesturePartResult.Fail;
    }

}

/// <summary>
/// The third part of the strong arm gesture
/// </summary>
public class WinningManSegment3 : IRelativeGestureSegment
{
    /// <summary>
    /// Checks the gesture.
    /// </summary>
    /// <param name="skeleton">The skeleton.</param>
    /// <returns>GesturePartResult based on if the gesture part has been completed</returns>
    public GesturePartResult CheckGesture(BasicAvatarModel skeleton)
    {
        Vector3 handRight = skeleton.getRawWorldPosition(JointType.HandRight);
        Vector3 handLeft = skeleton.getRawWorldPosition(JointType.HandLeft);
        Vector3 shoulderRight = skeleton.getRawWorldPosition(JointType.ShoulderRight);
        Vector3 shoulderLeft = skeleton.getRawWorldPosition(JointType.ShoulderLeft);
        Vector3 elbowRight = skeleton.getRawWorldPosition(JointType.ElbowRight);
        Vector3 elbowLeft = skeleton.getRawWorldPosition(JointType.ElbowLeft);
        Vector3 hipCenter = skeleton.getRawWorldPosition(JointType.SpineBase);
        Vector3 head = skeleton.getRawWorldPosition(JointType.Head);

        //Right Hand on the right side and left Hand left
        if (handRight.x > hipCenter.x && handLeft.x < hipCenter.x)
        {
            //Debug.Log("Start Winning Man 3");
            //Right Hand above Head and right of elbow
            if (handRight.y > head.y && handRight.x > elbowRight.x)
            {
                //Left Hand above Head and left of elbow
                if (handLeft.y > head.y && handLeft.x < elbowLeft.x)
                {
                    return GesturePartResult.Succeed;
                }
                return GesturePartResult.Pausing;
            }
            return GesturePartResult.Pausing;

        }
        //Debug.Log("3 Failed");
        return GesturePartResult.Fail;
    }
}
