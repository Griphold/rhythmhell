﻿using UnityEngine;
using Windows.Kinect;

/// <summary>
/// The first part of the AngryTableTop gesture
/// </summary>
public class AngryBabySegment1 : IRelativeGestureSegment
{
    public float checkDistance = 0.1f;
    /// <summary>
    /// Checks the gesture.
    /// </summary>
    /// <param name="skeleton">The skeleton.</param>
    /// <returns>GesturePartResult based on if the gesture part has been completed</returns>
    public GesturePartResult CheckGesture(BasicAvatarModel skeleton)
    {
        Vector3 handRight = skeleton.getRawWorldPosition(JointType.HandRight);
        Vector3 handLeft = skeleton.getRawWorldPosition(JointType.HandLeft);
        Vector3 elbowRight = skeleton.getRawWorldPosition(JointType.ElbowRight);
        Vector3 elbowLeft = skeleton.getRawWorldPosition(JointType.ElbowLeft);
        Vector3 shoulder = skeleton.getRawWorldPosition(JointType.SpineShoulder);
        Vector3 spineMid = skeleton.getRawWorldPosition(JointType.SpineMid);

        if (handRight.z > shoulder.z && handLeft.z > shoulder.z && handRight.y > spineMid.y && handLeft.y > spineMid.y && handRight.y > (elbowRight.y +checkDistance) && handLeft.y > (elbowLeft.y +checkDistance))
        {
            return GesturePartResult.Succeed;
        }
        return GesturePartResult.Fail;
    }
}
/// <summary>
/// The second part of the AngryTableTop gesture
/// </summary>
public class AngryBabySegment2 : IRelativeGestureSegment
{
    public float checkDistance = 0.1f;
    /// <summary>
    /// Checks the gesture.
    /// </summary>
    /// <param name="skeleton">The skeleton.</param>
    /// <returns>GesturePartResult based on if the gesture part has been completed</returns>
    public GesturePartResult CheckGesture(BasicAvatarModel skeleton)
    {
        Vector3 handRight = skeleton.getRawWorldPosition(JointType.HandRight);
        Vector3 handLeft = skeleton.getRawWorldPosition(JointType.HandLeft);
        Vector3 elbowRight = skeleton.getRawWorldPosition(JointType.ElbowRight);
        Vector3 elbowLeft = skeleton.getRawWorldPosition(JointType.ElbowLeft);
        Vector3 shoulder = skeleton.getRawWorldPosition(JointType.SpineShoulder);
        Vector3 spineMid = skeleton.getRawWorldPosition(JointType.SpineMid);

        if (handRight.z > shoulder.z && handLeft.z > shoulder.z)
        {
            if (handRight.y >= (elbowRight.y - checkDistance) && handRight.y <= (elbowRight.y + checkDistance) && handLeft.y >= (elbowRight.y - checkDistance) && handLeft.y <= (elbowRight.y + checkDistance))
            {
                return GesturePartResult.Succeed;
            }
            return GesturePartResult.Pausing;
        }
        return GesturePartResult.Fail;
    }
}
