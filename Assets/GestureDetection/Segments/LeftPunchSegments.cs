﻿using UnityEngine;
using Windows.Kinect;

/// <summary>
/// The first part of the RightPunch gesture
/// </summary>
public class LeftPunchSegment1 : IRelativeGestureSegment
{
    /// <summary>
    /// Checks the gesture.
    /// </summary>
    /// <param name="skeleton">The skeleton.</param>
    /// <returns>GesturePartResult based on if the gesture part has been completed</returns>
    public GesturePartResult CheckGesture(BasicAvatarModel skeleton)
    {
        Vector3 handRight = skeleton.getRawWorldPosition(JointType.HandRight);
        Vector3 handLeft = skeleton.getRawWorldPosition(JointType.HandLeft);
        Vector3 shoulderRight = skeleton.getRawWorldPosition(JointType.ShoulderRight);
        Vector3 shoulderLeft = skeleton.getRawWorldPosition(JointType.ShoulderLeft);
        Vector3 elbowRight = skeleton.getRawWorldPosition(JointType.ElbowRight);
        Vector3 elbowLeft = skeleton.getRawWorldPosition(JointType.ElbowLeft);
        Vector3 hipCenter = skeleton.getRawWorldPosition(JointType.SpineBase);
        Vector3 head = skeleton.getRawWorldPosition(JointType.Head);

        //Right Hand in front of right shoulder
        if (handLeft.z > shoulderLeft.z && handRight.x > hipCenter.x)
        {
            Vector3 handelbow = (handLeft - elbowLeft);
            Vector3 elbowshoulder = (shoulderLeft - elbowLeft);

            if (Vector3.Angle(handelbow, elbowshoulder) < 100.0f)
            {
                return GesturePartResult.Succeed;
            }
            return GesturePartResult.Pausing;
        }
        return GesturePartResult.Fail;
    }
}

/// <summary>
/// The second part of the RightPunch gesture
/// </summary>
public class LeftPunchSegment2 : IRelativeGestureSegment
{
    /// <summary>
    /// Checks the gesture.
    /// </summary>
    /// <param name="skeleton">The skeleton.</param>
    /// <returns>GesturePartResult based on if the gesture part has been completed</returns>
    public GesturePartResult CheckGesture(BasicAvatarModel skeleton)
    {
        Vector3 handRight = skeleton.getRawWorldPosition(JointType.HandRight);
        Vector3 handLeft = skeleton.getRawWorldPosition(JointType.HandLeft);
        Vector3 shoulderRight = skeleton.getRawWorldPosition(JointType.ShoulderRight);
        Vector3 shoulderLeft = skeleton.getRawWorldPosition(JointType.ShoulderLeft);
        Vector3 elbowRight = skeleton.getRawWorldPosition(JointType.ElbowRight);
        Vector3 elbowLeft = skeleton.getRawWorldPosition(JointType.ElbowLeft);
        Vector3 hipCenter = skeleton.getRawWorldPosition(JointType.SpineBase);
        Vector3 head = skeleton.getRawWorldPosition(JointType.Head);

        //Left Hand in front of shoulder
        if (handLeft.z > shoulderLeft.z && handRight.x > hipCenter.x)
        {
            Vector3 handelbow = (handLeft - elbowLeft);
            Vector3 elbowshoulder = (shoulderLeft - elbowLeft);

            if (Vector3.Angle(handelbow, elbowshoulder) > 130.0f)
            {
                return GesturePartResult.Succeed;
            }
            return GesturePartResult.Pausing;

        }
        return GesturePartResult.Fail;
    }

}
