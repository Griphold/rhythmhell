﻿using UnityEngine;
using Windows.Kinect;

/// <summary>
/// The first part of the Clap gesture
/// </summary>
public class ClapSegment1 : IRelativeGestureSegment
{
    public float checkDistance = 0.1f;
    /// <summary>
    /// Checks the gesture.
    /// </summary>
    /// <param name="skeleton">The skeleton.</param>
    /// <returns>GesturePartResult based on if the gesture part has been completed</returns>
    public GesturePartResult CheckGesture(BasicAvatarModel skeleton)
    {
        Vector3 handRight = skeleton.getRawWorldPosition(JointType.HandRight);
        Vector3 handLeft = skeleton.getRawWorldPosition(JointType.HandLeft);
        float dist = Vector3.Distance(handLeft, handRight);
        //Debug.Log("Part 1: " + dist);

        if (dist >= checkDistance)
        {
            return GesturePartResult.Succeed;
        }
        return GesturePartResult.Fail;
    }
}

/// <summary>
/// The second part of the Clap gesture
/// </summary>
public class ClapSegment2 : IRelativeGestureSegment
{
    public float checkDistance = 0.1f;
    /// <summary>
    /// Checks the gesture.
    /// </summary>
    /// <param name="skeleton">The skeleton.</param>
    /// <returns>GesturePartResult based on if the gesture part has been completed</returns>
    public GesturePartResult CheckGesture(BasicAvatarModel skeleton)
    {
        Vector3 handRight = skeleton.getRawWorldPosition(JointType.HandRight);
        Vector3 handLeft = skeleton.getRawWorldPosition(JointType.HandLeft);
        float dist = Vector3.Distance(handLeft, handRight);
        //Debug.Log("Part 2: "+dist);
        if (dist >= checkDistance)
        {
            return GesturePartResult.Pausing;
        }
        else
        {
            return GesturePartResult.Succeed;
        }

    }
}
