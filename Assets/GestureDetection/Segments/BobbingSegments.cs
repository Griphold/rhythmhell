﻿using UnityEngine;
using Windows.Kinect;



/// <summary>
/// The first part of the RightPunch gesture
/// </summary>
public class BobbingSegment1 : IRelativeGestureSegment
{
    public static Vector3 headPos;

    /// <summary>
    /// Checks the gesture.
    /// </summary>
    /// <param name="skeleton">The skeleton.</param>
    /// <returns>GesturePartResult based on if the gesture part has been completed</returns>
    public GesturePartResult CheckGesture(BasicAvatarModel skeleton)
    {
        Vector3 handRight = skeleton.getRawWorldPosition(JointType.HandRight);
        Vector3 handLeft = skeleton.getRawWorldPosition(JointType.HandLeft);
        Vector3 shoulderRight = skeleton.getRawWorldPosition(JointType.ShoulderRight);
        Vector3 shoulderLeft = skeleton.getRawWorldPosition(JointType.ShoulderLeft);
        Vector3 elbowRight = skeleton.getRawWorldPosition(JointType.ElbowRight);
        Vector3 elbowLeft = skeleton.getRawWorldPosition(JointType.ElbowLeft);
        Vector3 hipCenter = skeleton.getRawWorldPosition(JointType.SpineBase);
        Vector3 head = skeleton.getRawWorldPosition(JointType.Head);
        Vector3 kneeLeft = skeleton.getRawWorldPosition(JointType.KneeLeft);
        Vector3 kneeRight = skeleton.getRawWorldPosition(JointType.KneeRight);
        Vector3 spineMid = skeleton.getRawWorldPosition(JointType.SpineMid);

        Vector2 toSpine = (new Vector2(spineMid.y, spineMid.z) - new Vector2(hipCenter.y, hipCenter.z)).normalized;
        Vector2 toLeftKnee = (new Vector2(kneeLeft.y, kneeLeft.z) - new Vector2(hipCenter.y, hipCenter.z)).normalized;
        Vector2 toRightKnee = (new Vector2(kneeRight.y, kneeRight.z) - new Vector2(hipCenter.y, hipCenter.z)).normalized;
        float leftKneeAngle = Vector2.Angle(toSpine, toLeftKnee);
        float rightKneeAngle = Vector2.Angle(toSpine, toRightKnee);
        float averageKnee = 0.5f * (leftKneeAngle + rightKneeAngle);
        //Debug.Log("Angle: " + averageKnee);
        if (averageKnee > 165)
        {
            return GesturePartResult.Succeed;
        }
        else
            return GesturePartResult.Pausing;
    }
}

/// <summary>
/// The second part of the RightPunch gesture
/// </summary>
public class BobbingSegment2 : IRelativeGestureSegment
{

    /// <summary>
    /// Checks the gesture.
    /// </summary>
    /// <param name="skeleton">The skeleton.</param>
    /// <returns>GesturePartResult based on if the gesture part has been completed</returns>
    public GesturePartResult CheckGesture(BasicAvatarModel skeleton)
    {
        Vector3 handRight = skeleton.getRawWorldPosition(JointType.HandRight);
        Vector3 handLeft = skeleton.getRawWorldPosition(JointType.HandLeft);
        Vector3 shoulderRight = skeleton.getRawWorldPosition(JointType.ShoulderRight);
        Vector3 shoulderLeft = skeleton.getRawWorldPosition(JointType.ShoulderLeft);
        Vector3 elbowRight = skeleton.getRawWorldPosition(JointType.ElbowRight);
        Vector3 elbowLeft = skeleton.getRawWorldPosition(JointType.ElbowLeft);
        Vector3 hipCenter = skeleton.getRawWorldPosition(JointType.SpineBase);
        Vector3 head = skeleton.getRawWorldPosition(JointType.Head);
        Vector3 kneeLeft = skeleton.getRawWorldPosition(JointType.KneeLeft);
        Vector3 kneeRight = skeleton.getRawWorldPosition(JointType.KneeRight);
        Vector3 spineMid = skeleton.getRawWorldPosition(JointType.SpineMid);

        Vector2 toSpine = (new Vector2(spineMid.y, spineMid.z) - new Vector2(hipCenter.y, hipCenter.z)).normalized;
        Vector2 toLeftKnee = (new Vector2(kneeLeft.y, kneeLeft.z) - new Vector2(hipCenter.y, hipCenter.z)).normalized;
        Vector2 toRightKnee = (new Vector2(kneeRight.y, kneeRight.z) - new Vector2(hipCenter.y, hipCenter.z)).normalized;
        float leftKneeAngle = Vector2.Angle(toSpine, toLeftKnee);
        float rightKneeAngle = Vector2.Angle(toSpine, toRightKnee);
        float averageKnee = 0.5f * (leftKneeAngle + rightKneeAngle);
        //Debug.Log("Angle: " + averageKnee);

        if (averageKnee < 160){
            return GesturePartResult.Succeed;
        }
        else
            return GesturePartResult.Pausing;
    }
}
/*
/// <summary>
/// The second part of the RightPunch gesture
/// </summary>
public class BobbingSegment3 : IRelativeGestureSegment
{

   /// <summary>
   /// Checks the gesture.
   /// </summary>
   /// <param name="skeleton">The skeleton.</param>
   /// <returns>GesturePartResult based on if the gesture part has been completed</returns>
   public GesturePartResult CheckGesture(BasicAvatarModel skeleton)
   {
       Vector3 head = skeleton.getRawWorldPosition(JointType.Head);

       if (BobbingSegment1.headPos.y > head.y + 0.01f)
       {
           return GesturePartResult.Pausing;
       }
       else
       {
           Debug.Log("Ende: " + head.y);
           return GesturePartResult.Succeed;
       }
   }
}
*/