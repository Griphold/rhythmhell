﻿using UnityEngine;
using Windows.Kinect;

/// <summary>
/// The first part of the strong arm gesture
/// </summary>
public class StrongArmSegment1 : IRelativeGestureSegment
{
    /// <summary>
    /// Checks the gesture.
    /// </summary>
    /// <param name="skeleton">The skeleton.</param>
    /// <returns>GesturePartResult based on if the gesture part has been completed</returns>
    public GesturePartResult CheckGesture(BasicAvatarModel skeleton)
    {
        Vector3 handRight = skeleton.getRawWorldPosition(JointType.HandRight);
        Vector3 shoulderRight = skeleton.getRawWorldPosition(JointType.ShoulderRight);
        Vector3 elbowRight = skeleton.getRawWorldPosition(JointType.ElbowRight);

        //Right Hand under arm
        if (handRight.y < shoulderRight.y)
        {
            //Debug.Log("Start Strong Arm 1");
            //Right Hand between Shoulder and Elbow
            if (handRight.x < elbowRight.x && handRight.x > shoulderRight.x)
            {
                //Debug.Log("Strong Arm 1 Succeed");
                return GesturePartResult.Succeed;
            }

            return GesturePartResult.Pausing;
        }
        return GesturePartResult.Fail;
    }
}

/// <summary>
/// The second part of the strong arm gesture
/// </summary>
public class StrongArmSegment2 : IRelativeGestureSegment
{
    /// <summary>
    /// Checks the gesture.
    /// </summary>
    /// <param name="skeleton">The skeleton.</param>
    /// <returns>GesturePartResult based on if the gesture part has been completed</returns>
    public GesturePartResult CheckGesture(BasicAvatarModel skeleton)
    {
        Vector3 handRight = skeleton.getRawWorldPosition(JointType.HandRight);
        Vector3 shoulderRight = skeleton.getRawWorldPosition(JointType.ShoulderRight);
        Vector3 elbowRight = skeleton.getRawWorldPosition(JointType.ElbowRight);

        //Right Hand right of shoulder
        if (handRight.x > shoulderRight.x)
        {
            //Debug.Log("Start Strong Arm 2");
            //Right Hand above elbow
            if (handRight.y > elbowRight.y && handRight.x > elbowRight.x)
            {
                //Debug.Log("Strong Arm 2 Succeed");
                return GesturePartResult.Succeed;
            }
            return GesturePartResult.Pausing;
        }
        //Debug.Log("2 Failed");
        return GesturePartResult.Fail;
    }

}

/// <summary>
/// The third part of the strong arm gesture
/// </summary>
public class StrongArmSegment3 : IRelativeGestureSegment
{
    /// <summary>
    /// Checks the gesture.
    /// </summary>
    /// <param name="skeleton">The skeleton.</param>
    /// <returns>GesturePartResult based on if the gesture part has been completed</returns>
    public GesturePartResult CheckGesture(BasicAvatarModel skeleton)
    {
        Vector3 handRight = skeleton.getRawWorldPosition(JointType.HandRight);
        Vector3 shoulderRight = skeleton.getRawWorldPosition(JointType.ShoulderRight);
        Vector3 elbowRight = skeleton.getRawWorldPosition(JointType.ElbowRight);

        //Right Hand above elbow 
        if (handRight.y > elbowRight.y )
        {
            //Debug.Log("Start Strong Arm 3");
            //Right Hand between Shoulder and Elbow and elbow under Shoulder
            if (handRight.x > shoulderRight.x && handRight.x < elbowRight.x && elbowRight.y < shoulderRight.y)
            {
                //Debug.Log("Strong Arm 3 Succeed");
                return GesturePartResult.Succeed;
            }
            return GesturePartResult.Pausing;

        }
        //Debug.Log("3 Failed");
        return GesturePartResult.Fail;
    }
}
