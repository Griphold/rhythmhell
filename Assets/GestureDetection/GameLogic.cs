﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GameLogic : MonoBehaviour
{

    public GestureController gc;
    public List<Gesture.Type> activeGestures;
    public float KinectDelay = 100;
    public bool UpdateGestures;

    //sound effects
    private UnityEngine.AudioSource audioSource;
    public AudioClip flapSFX;
    public AudioClip[] punchSFX;
    public AudioClip[] angryBabySFX;

    private IRelativeGestureSegment[] clap = { new ClapSegment1(), new ClapSegment2() };
    private IRelativeGestureSegment[] rightPunch = { new RightPunchSegment1(), new RightPunchSegment2() };
    private IRelativeGestureSegment[] leftPunch = { new LeftPunchSegment1(), new LeftPunchSegment2() };
    private IRelativeGestureSegment[] bobbing = { new BobbingSegment1(), new BobbingSegment2() };
    private IRelativeGestureSegment[] angryBaby = { new AngryBabySegment1(), new AngryBabySegment2() };
    private IRelativeGestureSegment[] flap = { new FlapSegment1(), new FlapSegment2() };

    private List<Beat> processedBeats = new List<Beat>();

    // Use this for initialization
    void Start()
    {
        gc.GestureRecognizedInController += OnGestureRecognized;

        if (!UpdateGestures)
        {
            foreach (Gesture.Type g in activeGestures)
            {
                AddGesture(g);
            }
        }

        TrackManager.KinectDelay = KinectDelay;

        audioSource = GetComponent<UnityEngine.AudioSource>(); 
    }

    private void AddGesture(Gesture.Type t)
    {
        foreach (Gesture.Type g in activeGestures)
        {
            switch (g)
            {
                case Gesture.Type.Clap:
                    gc.AddGesture(g, clap);
                    break;

                case Gesture.Type.RightPunch:
                    gc.AddGesture(g, rightPunch);
                    break;

                case Gesture.Type.LeftPunch:
                    gc.AddGesture(g, leftPunch);
                    break;

                case Gesture.Type.Bobbing:
                    gc.AddGesture(g, bobbing);
                    break;

                case Gesture.Type.AngryBaby:
                    gc.AddGesture(g, angryBaby);
                    break;
                case Gesture.Type.Flap:
                    gc.AddGesture(g, flap);
                    break;
            }

        }
    }

    void OnGestureRecognized(object sender, GestureEventArgs e)
    {
        switch (e.GestureName)
        {
            case Gesture.Type.Clap:
                //Debug.Log("Clap Recognized");
                break;

            case Gesture.Type.RightPunch:
                if (punchSFX.Length > 0)
                    audioSource.PlayOneShot(punchSFX[Random.Range(0, punchSFX.Length)]);

                //Debug.Log("RightPunch Recognized at " /*+ Time.time*/);
                break;

            case Gesture.Type.LeftPunch:
                if (punchSFX.Length > 0)
                    audioSource.PlayOneShot(punchSFX[Random.Range(0, punchSFX.Length)]);

                //Debug.Log("LeftPunch Recognized at " /*+ Time.time*/);
                break;

            case Gesture.Type.Bobbing:
                //Debug.Log("Bobbing Recognized at " /*+ Time.time*/);
                break;

            case Gesture.Type.AngryBaby:
                if(angryBabySFX.Length > 0)
                    audioSource.PlayOneShot(angryBabySFX[Random.Range(0, angryBabySFX.Length)]);
                //Debug.Log("Angry Baby Recognized at " /*+ Time.time*/);
                break;
            case Gesture.Type.Flap:
                //Debug.Log("Flap Recognized at "/* + Time.time*/);
                audioSource.PlayOneShot(flapSFX);
                break;
        }

        TrackManager.Instance.OnGestureReceived(e.GestureName);
    }

    // Update is called once per frame
    void Update()
    {
        if (UpdateGestures)
        {
            Beat b = TrackManager.Instance.GetNextBeat(true);
            //bool cleared = false;

            if (b != null && !processedBeats.Contains(b))
            {
                List<Gesture.Type> gestures = b.AcceptedGestures;


                activeGestures.Clear();
                activeGestures.AddRange(gestures);

                //Debug.Log("Updated");

                gc.RemoveAllGestures();

                foreach (Gesture.Type g in gestures)
                {
                    AddGesture(g);
                }


                processedBeats.Add(b);

                /*
                foreach (Gesture.Type g in gestures)
                {
                    
                    if (!gc.Contains(g))
                    {
                        if (!cleared)
                        {
                            gc.RemoveAllGestures();
                            Clear();
                            cleared = true;
                        }

                        

                        AddGesture(g);
                        activeGestures.Add(g);
                    }
                }*/
            }
        }
    }

    private void Clear()
    {
        activeGestures.Clear();
    }
}
