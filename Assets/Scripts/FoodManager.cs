﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class FoodManager : MonoBehaviour
{
    public GameObject VegetablePrefab;
    public Transform VegetableSpawnPoint;
    public GameObject CandyPrefab;
    public Transform CandySpawnPoint;
    public float SpawnTimeInMs;
    
    private List<Beat> spawnBeats = new List<Beat>();

    void Start()
    {
        spawnBeats.AddRange(TrackManager.Instance.GetActiveTrack().Beats);
    }

    // Update is called once per frame
    void Update()
    {
        //spawn objects when spawntime is reached
        float currentTime = TrackManager.Instance.GetActiveTrackTimeInMs();
        for (int i = spawnBeats.Count - 1; i >= 0; i--)
        {
            Beat b = spawnBeats[i];
            float timeUntilBeat = b.Time - currentTime;

            if (timeUntilBeat < SpawnTimeInMs)
            {
                if (b.type == Beat.Type.Vegetable)
                {
                    SpawnVegetable(b);
                }
                else
                {
                    SpawnCandy(b);
                }
                spawnBeats.RemoveAt(i);
            }
        }
    }

    private void SpawnCandy(Beat beat)
    {
        GameObject candyObj = (GameObject)GameObject.Instantiate(CandyPrefab, CandySpawnPoint.position, Quaternion.identity);
        CandyScript candy = candyObj.GetComponent<CandyScript>();

        candy.SetBeat(beat);
        candy.SetPlatePosition(transform.position);
    }

    private void SpawnVegetable(Beat beat)
    {
        GameObject veggieObj = (GameObject)GameObject.Instantiate(VegetablePrefab, VegetableSpawnPoint.position, Quaternion.identity);
        VegetableScript veggie = veggieObj.GetComponent<VegetableScript>();

        veggie.SetBeat(beat);
        veggie.SetPlatePosition(transform.position);
    }
}
