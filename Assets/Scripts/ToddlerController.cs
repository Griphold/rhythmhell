﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Windows.Kinect;
using System;

public class ToddlerController : MonoBehaviour
{
    //TODo create circularbuffer for smoothing joint movement

    // all kinect joints as transform objects
    public Transform ShoulderLeft;
    public Transform ElbowLeft;
    public Transform WristLeft;

    public Transform ShoulderRight;
    public Transform ElbowRight;
    public Transform WristRight;

    public Transform Head;
    public Transform Neck;

    // root transformation, used to determine the initial rotation of the complete model
    public Transform RootTransform;

    // avatar of the motion capturing data
    public BasicAvatarModel MoCapAvatar;

    public Transform EyeLeft;
    public Transform EyeRight;
    public float SquintScaleY;
    public float SquintRotationZ;
    public float SquintPositionY;

    // dict of all the joint-transforms that are available in the model
    protected Dictionary<JointType, Transform> armJoints;

    // initial joint rotations of the model (the rotations are "local rotations" relative to the RootTransform rotation; see Start function for further details)
    protected Dictionary<JointType, Quaternion> initialArmJointRotations = new Dictionary<JointType, Quaternion>();

    private float EyesTimer = 0;
    private Vector3 InitialEulerEyeLeft;
    private Vector3 InitialEulerEyeRight;
    private Vector3 InitialPositionEyeLeft;
    private Vector3 InitialPositionEyeRight;
    

    public virtual void Start()
    {
        armJoints = new Dictionary<JointType, Transform>() 
        { 
            {JointType.ShoulderLeft, ShoulderLeft}, 
            {JointType.ElbowLeft, ElbowLeft}, 
            {JointType.ShoulderRight, ShoulderRight}, 
            {JointType.ElbowRight, ElbowRight},
            {JointType.Head, Head}
        };


        // compute initial rotation of the joints of the model
        // Note: because we want the rotation to be relative to Quaternion.identity (no rotation), we compute a "local rotation" relative to the RootTransform rotation of the model
        foreach (JointType jt in armJoints.Keys)
        {
            initialArmJointRotations[jt] = Quaternion.Inverse(RootTransform.rotation) * armJoints[jt].rotation;
        }

        TrackManager.Instance.BeatOverEvent += OnBeatOver;

        InitialEulerEyeLeft = EyeLeft.localEulerAngles;
        InitialEulerEyeRight = EyeRight.localEulerAngles;
        InitialPositionEyeLeft = EyeLeft.localPosition;
        InitialPositionEyeRight = EyeRight.localPosition;
    }

    // Update rotation of all known joints
    public virtual void Update()
    {

        //map kinect rotations with linear interpolation to arm joints 
        foreach (JointType jt in armJoints.Keys)
        {
            // the applyRelativeRotationChange function returns the new "local rotation" relative to the RootTransform Rotation...
            Quaternion localRotTowardsRootTransform = MoCapAvatar.applyRelativeRotationChange(jt, initialArmJointRotations[jt]);

            // ...therefore we have to multiply it with the RootTransform Rotation to get the global rotation of the joint
            armJoints[jt].rotation = Quaternion.Slerp(armJoints[jt].rotation, RootTransform.rotation * localRotTowardsRootTransform, Time.deltaTime * 40.0f);
        }

        if (EyesTimer > 0)
        {
            EyesTimer -= Time.deltaTime;
        }
        else
        {
            SetNormalEyes();
        }
    }

    public void OnBeatOver(object sender, BeatEventArgs e)
    {
        EyeLeft.localScale = new Vector3(1, SquintScaleY, 1);
        EyeRight.localScale = new Vector3(1, SquintScaleY, 1);

        EyeLeft.localEulerAngles = new Vector3(InitialEulerEyeLeft.x, InitialEulerEyeLeft.y, -SquintRotationZ);
        EyeRight.localEulerAngles = new Vector3(InitialEulerEyeRight.x, InitialEulerEyeRight.y, SquintRotationZ);

        EyeLeft.localPosition = InitialPositionEyeLeft + new Vector3(0, SquintPositionY, 0);
        EyeRight.localPosition = InitialPositionEyeRight + new Vector3(0, SquintPositionY, 0);

        EyesTimer = 0.8f;
    }

    private void SetNormalEyes()
    {
        EyeLeft.localScale = new Vector3(1, 1, 1);
        EyeRight.localScale = new Vector3(1, 1, 1);

        EyeLeft.localEulerAngles = InitialEulerEyeLeft;
        EyeRight.localEulerAngles = InitialEulerEyeRight;

        EyeLeft.localPosition = InitialPositionEyeLeft;
        EyeRight.localPosition = InitialPositionEyeRight;
    }
}
