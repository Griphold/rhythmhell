﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class DestroyParticleEffect : MonoBehaviour 
{
    private ParticleSystem[] childPartSys;
	// Use this for initialization
	void Start () 
    {
        childPartSys = GetComponentsInChildren<ParticleSystem>();
	
	}
	
	// Update is called once per frame
	void Update () 
    {
        bool allOver = true;

        foreach (ParticleSystem p in childPartSys)
        {
            if (p.IsAlive()) { allOver = false; }
        }

        if (allOver) { Destroy(this.gameObject); } 
	}
}
