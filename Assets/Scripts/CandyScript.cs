﻿using UnityEngine;
using System.Collections;

public class CandyScript : MonoBehaviour 
{
    public GameObject DestroyEffect;

    public AnimationCurve XCurve;
    public AnimationCurve YCurve;
    private Vector3 platePosition;
    private Beat beat;
    private Vector3 spawnPosition;
    private float initialTimeUntilBeat;

	// Use this for initialization
	void Start () 
    {
        spawnPosition = transform.position;
        initialTimeUntilBeat = (beat.Time) - TrackManager.Instance.GetActiveTrackTimeInMs(); 
	}
	
	// Update is called once per frame
	void Update () 
    {
        //get info about beat timing
        float currentTime = TrackManager.Instance.GetActiveTrackTimeInMs();
        float timeUntilBeat = (beat.Time) - currentTime;
        float progress = (initialTimeUntilBeat - timeUntilBeat) / (2 * initialTimeUntilBeat);

        float xScale = Mathf.Abs(spawnPosition.x - platePosition.x) * 2.0f;
        float yScale = Mathf.Abs(spawnPosition.y - platePosition.y);

        //get x and y position according to progress & animation
        float x = spawnPosition.x + XCurve.Evaluate(progress) * xScale;
        float y = platePosition.y + YCurve.Evaluate(progress) * yScale;

        transform.position = new Vector3(x, y, transform.position.z);

        //Destroy this if animation over
        if (progress > 1.0f)
        {
            Destroy(this.gameObject);
        }

        if (beat.Status == BeatStatus.Success)
        {
            BeatSuccess();
        }
	}

    private void BeatSuccess()
    {
        GameObject.Instantiate(DestroyEffect, transform.position, Quaternion.identity);
        Destroy(this.gameObject);
    }

    public void SetBeat(Beat nextBeat)
    {
        beat = nextBeat;
    }

    public void SetPlatePosition(Vector3 position)
    {
        platePosition = position;
    }
}
