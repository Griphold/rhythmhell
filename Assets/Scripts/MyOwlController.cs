﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Windows.Kinect;

public class MyOwlController : MonoBehaviour
{

    public Transform leftWing;
    public Transform rightWing;

    // avatar of the motion capturing data
    public BasicAvatarModel MoCapAvatar;

    public Vector3 squishScale;
    public Vector3 squishPosition;

    private Quaternion leftWingStartRotation;
    private Quaternion rightWingStartRotation;
    private Vector3 startScale;
    private Vector3 startPosition;

    public virtual void Start()
    {
        leftWingStartRotation = leftWing.rotation;
        rightWingStartRotation = rightWing.rotation;

        startScale = transform.localScale;
        startPosition = transform.position;
    }

    // Update rotation of all known joints
    public virtual void Update()
    {
        //leftWing.Rotate(0, 0, 30 * Time.deltaTime);
        //rightWing.Rotate(0, 0, -30 * Time.deltaTime);

        //Rotate Wings
        Vector3 leftShoulder = MoCapAvatar.getRawWorldPosition(JointType.ShoulderLeft);
        Vector3 rightShoulder = MoCapAvatar.getRawWorldPosition(JointType.ShoulderRight);
        Vector3 leftElbow = MoCapAvatar.getRawWorldPosition(JointType.ElbowLeft);
        Vector3 rightElbow = MoCapAvatar.getRawWorldPosition(JointType.ElbowRight);

        Vector2 lShoulderToElbow = new Vector2(leftElbow.x - leftShoulder.x, leftElbow.y - leftShoulder.y);
        Vector2 rShoulderToElbow = new Vector2(rightElbow.x - rightShoulder.x, rightElbow.y - rightShoulder.y);

        float angleLeft = Vector2.Angle(Vector2.down, lShoulderToElbow);
        float angleRight = Vector2.Angle(Vector2.down, rShoulderToElbow);

        leftWing.rotation = Quaternion.Slerp(leftWing.rotation, leftWingStartRotation * Quaternion.AngleAxis(-angleLeft, Vector3.back), Time.deltaTime * 60);
        rightWing.rotation = Quaternion.Slerp(rightWing.rotation, rightWingStartRotation * Quaternion.AngleAxis(angleRight, Vector3.back), Time.deltaTime * 60);

        //squish character
        Vector3 hipCenter = MoCapAvatar.getRawWorldPosition(JointType.SpineBase);
        Vector3 kneeLeft = MoCapAvatar.getRawWorldPosition(JointType.KneeLeft);
        Vector3 kneeRight = MoCapAvatar.getRawWorldPosition(JointType.KneeRight);
        Vector3 spineMid = MoCapAvatar.getRawWorldPosition(JointType.SpineMid);

        Vector2 toSpine = (new Vector2(spineMid.y, spineMid.z) - new Vector2(hipCenter.y, hipCenter.z)).normalized;
        Vector2 toLeftKnee = (new Vector2(kneeLeft.y, kneeLeft.z) - new Vector2(hipCenter.y, hipCenter.z)).normalized;
        Vector2 toRightKnee = (new Vector2(kneeRight.y, kneeRight.z) - new Vector2(hipCenter.y, hipCenter.z)).normalized;
        float leftKneeAngle = Vector2.Angle(toSpine, toLeftKnee);
        float rightKneeAngle = Vector2.Angle(toSpine, toRightKnee);
        float averageKnee = 0.5f * (leftKneeAngle + rightKneeAngle);

        float squishValue = Mathf.Clamp01((180 - averageKnee) / 30f);
        //float squishValue = 0;

        transform.localScale = Vector3.Lerp(startScale, squishScale, squishValue);
        transform.position = Vector3.Lerp(startPosition, squishPosition, squishValue);
    }
}
