﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Rigidbody))]
public class VegetableScript : MonoBehaviour 
{
    //Effects
    public GameObject DestroyEffect;

    //Animation
    public AnimationCurve ScaleCurve;
    public AnimationCurve ZCurve;
    public AnimationCurve YCurve;
    private Vector3 platePosition;
    private Vector3 spawnPosition;
    private Vector3 originalScale;

    //timing
    private Beat beat;
    private float initialTimeUntilBeat;
    private bool hit = false;
    
    //physics
    private Rigidbody rigidBody;

	// Use this for initialization
	void Start () 
    {
        spawnPosition = transform.position;
        initialTimeUntilBeat = (beat.Time + TrackManager.KinectDelay) - TrackManager.Instance.GetActiveTrackTimeInMs();

        originalScale = transform.localScale;

        rigidBody = GetComponent<Rigidbody>();
	}
	
	// Update is called once per frame
	void Update () 
    {
        if (beat.Status == BeatStatus.Failed)
        {

            //get info about beat timing
            float currentTime = TrackManager.Instance.GetActiveTrackTimeInMs();
            float timeUntilBeat = (beat.Time + TrackManager.KinectDelay) - currentTime;
            float progress = (initialTimeUntilBeat - timeUntilBeat) / (2 * initialTimeUntilBeat);

            float zScale = Mathf.Abs(spawnPosition.z - platePosition.z) * 2.0f;
            float yScale = Mathf.Abs(spawnPosition.y - platePosition.y);

            //calculate z and y positions
            float z = spawnPosition.z + ZCurve.Evaluate(progress) * zScale;
            float y = platePosition.y + YCurve.Evaluate(progress) * yScale;

            transform.position = new Vector3(transform.position.x, y, z);
            //transform.localScale = originalScale * (1.0f - progress);//scale according to inverse progress (because of orthographic camera)

            //if animation finished, destroy this object
            if (progress > 0.675f)
            {
                GameObject.Instantiate(DestroyEffect, transform.position, Quaternion.identity);
                Destroy(this.gameObject);
            }
        }
        else if (hit == false)
        {
            //repel the vegetable if hit
            rigidBody.isKinematic = false;

            Vector3 randomVel = new Vector3(Random.Range(50.0f, 200.0f) * Mathf.Sign(Random.Range(-1.0f, 1.0f)), Random.Range(0.0f, 20.0f), -100.0f);
            rigidBody.velocity = randomVel * 0.1f;

            hit = true;
        }



        //delete if behind camera and hit
        if (hit && transform.position.z < Camera.main.transform.position.z)
        {
            Destroy(this.gameObject);
        }


        //resize depending on z position (to simulate depth in orthographic camera)
        //float zDist_Max = Mathf.Abs(platePosition.z - spawnPosition.z) * 2.0f;
        //float zDist = transform.position.z - spawnPosition.z;
        //transform.localScale = originalScale * ScaleCurve.Evaluate(zDist/zDist_Max);
	}

    public void SetBeat(Beat nextBeat)
    {
        beat = nextBeat;
    }

    public void SetPlatePosition(Vector3 position)
    {
        platePosition = position;
    }
}
