﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Windows.Kinect;
using System;

public class FlySwatterAvatarController : MonoBehaviour
{
    //TODo create circularbuffer for smoothing joint movement

    // all kinect joints as transform objects
    public Transform ShoulderLeft;
    public Transform ElbowLeft;
    public Transform WristLeft;

    public Transform ShoulderRight;
    public Transform ElbowRight;
    public Transform WristRight;


    // root transformation, used to determine the initial rotation of the complete model
    public Transform RootTransform;

    // avatar of the motion capturing data
    public BasicAvatarModel MoCapAvatar;

    // dict of all the joint-transforms that are available in the model
    protected Dictionary<JointType, Transform> armJoints;

    // initial joint rotations of the model (the rotations are "local rotations" relative to the RootTransform rotation; see Start function for further details)
    protected Dictionary<JointType, Quaternion> initialArmJointRotations = new Dictionary<JointType, Quaternion>();

    public virtual void Start()
    {
        armJoints = new Dictionary<JointType, Transform>() 
        { 
            {JointType.ShoulderLeft, ShoulderLeft}, 
            {JointType.ElbowLeft, ElbowLeft}, 
            {JointType.ShoulderRight, ShoulderRight}, 
            {JointType.ElbowRight, ElbowRight},
            {JointType.WristRight, WristRight},
            {JointType.WristLeft, WristLeft}
        };


        // compute initial rotation of the joints of the model
        // Note: because we want the rotation to be relative to Quaternion.identity (no rotation), we compute a "local rotation" relative to the RootTransform rotation of the model
        foreach (JointType jt in armJoints.Keys)
        {
            initialArmJointRotations[jt] = Quaternion.Inverse(RootTransform.rotation) * armJoints[jt].rotation;
        }
    }

    // Update rotation of all known joints
    public virtual void Update()
    {
        
        //map kinect rotations with linear interpolation to arm joints 
        foreach (JointType jt in armJoints.Keys)
        {
            // the applyRelativeRotationChange function returns the new "local rotation" relative to the RootTransform Rotation...
            Quaternion localRotTowardsRootTransform = MoCapAvatar.applyRelativeRotationChange(jt, initialArmJointRotations[jt]);
            
            // ...therefore we have to multiply it with the RootTransform Rotation to get the global rotation of the joint
            armJoints[jt].rotation = Quaternion.Slerp(armJoints[jt].rotation, RootTransform.rotation * localRotTowardsRootTransform, Time.deltaTime * 40.0f);
        }    
        
        /*
        //calculate wrist rotation
        Vector3 normal = Vector3.Cross(WristLeft.up, WristRight.up);
        WristRight.LookAt(WristRight.position - normal, WristRight.up);
        WristLeft.LookAt(WristLeft.position - normal, WristLeft.up);*/

        //connection vectors between wrists
        Vector3 leftToRightWrist = WristRight.position - WristLeft.position;
        Vector3 RightToLeftWrist = WristLeft.position - WristRight.position;

        //project connection vector in hand x-z-plane, up vector of hand defines the normal
        Vector3 projected_right = Vector3.ProjectOnPlane(RightToLeftWrist, WristRight.up);
        Vector3 projected_left = Vector3.ProjectOnPlane(-leftToRightWrist, WristLeft.up);

        //calculate the angle between the projected vector and the current wrist rotation
        float angle_right = Vector3.Angle(projected_right, WristRight.right);
        float angle_left = Vector3.Angle(projected_left, -WristLeft.right);

        //determine if positive or negative angle
        Vector3 local_projected_right = WristRight.InverseTransformDirection(projected_right);
        Vector3 local_projected_left = WristLeft.InverseTransformDirection(projected_left);

        angle_right = local_projected_right.z < 0 ? angle_right : -angle_right;
        angle_left = local_projected_left.z > 0 ? angle_left : -angle_left;

        //rotate the wrists so they face each other
        WristRight.Rotate(0, angle_right + 180.0f, 0, Space.Self);
        WristLeft.Rotate(0, angle_left, 0, Space.Self);
    }
}
