﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(Collider))]
public class Fly : MonoBehaviour
//TODO change order in layer when fly is behind table
{
    private Transform bottomLeft, bottomRight, topLeft, topRight;//corners of the rectangle to fly towards
    private Transform danceBottomLeft, danceBottomRight, danceTopLeft, danceTopRight;

    public float flySpeed = 5.0f;
    public AnimationCurve ProgressByTime;
    public Sprite KOSprite;
    public SpriteRenderer FaceRenderer;
    public Animator WingAnimator;
    public AudioClip FlySFX;
    public AudioClip[] CoughSFX;
    public GameObject DeadParticleSystem;

    private Vector3 targetPos;//position to hit fly at
    private Vector3 startPos;

    private enum State
    {
        Dancing,
        Flying,
        Hit,
        NotHit
    }

    private State state = State.Dancing;

    private Beat beat;//each fly only reacts to one beat
    private float gestureTimeWindow;

    private Rigidbody rigidBody;
    private bool flySwatted = false;

    private bool PlayingCough = false;

    //dance stuff
    private float danceStartTime;
    private int currentTick;
    private float tickInterval;

    void Start()
    {
        beat = TrackManager.Instance.GetNextBeat();
        gestureTimeWindow = TrackManager.Instance.GetActiveTrack().GestureTimeWindow;

        if (beat == null) Debug.LogError("Fly has no beat!");


        rigidBody = GetComponent<Rigidbody>();



        //start "dancing"
        float currentTime = TrackManager.Instance.GetActiveTrackTimeInMs();
        float timeUntilBeat = beat.Time - currentTime;

        StartCoroutine(RandomDanceRoutine(timeUntilBeat - 2000, 8));
    }

    void Update()
    {
        //always look at camera
        transform.LookAt(Camera.main.transform);


        float currentTime = TrackManager.Instance.GetActiveTrackTimeInMs();
        float timeUntilBeat = beat.Time - currentTime;

        switch (state)
        {
            case State.Dancing:




                float dance_progress = (currentTime - (danceStartTime + currentTick * tickInterval)) / tickInterval;
                transform.position = Vector3.Lerp(startPos, targetPos, dance_progress);//move towards hit position


                //start flying towards hit position
                if (timeUntilBeat < 2000)
                {

                    targetPos = calculateRandomPos(bottomLeft.position, topLeft.position, topRight.position, bottomRight.position);
                    startPos = transform.position;
                    state = State.Flying;


                    if(FlySFX != null) GetComponent<AudioSource>().PlayOneShot(FlySFX);
                }


                break;
            case State.Flying:
                {
                    float progress = (2000 - timeUntilBeat) / 2000.0f;
                    transform.position = Vector3.Lerp(startPos, targetPos, ProgressByTime.Evaluate(progress));//move towards hit position
                    //player has hit the beat
                    if (beat.Status == BeatStatus.Success)
                    {
                        //player has also hit the fly
                        if (flySwatted)
                        {
                            state = State.Hit;
                            gameObject.GetComponent<Collider>().isTrigger = false;
                            rigidBody.isKinematic = false;
                            FaceRenderer.sprite = KOSprite;
                            WingAnimator.SetBool("dead", true);
                            GameObject.Instantiate(DeadParticleSystem, transform.position, Quaternion.identity);
                            Destroy(this.gameObject);
                        }
                        //player has not hit the fly
                        else
                        {
                            state = State.NotHit;
                            targetPos = Camera.main.transform.position;
                            beat.Status = BeatStatus.Failed;
                        }
                    }
                    //player has not hit the beat
                    else
                    {
                        if (currentTime > beat.Time + gestureTimeWindow)//beat is over 
                        {
                            state = State.NotHit;
                            targetPos = Camera.main.transform.position;
                        }
                    }
                }

                break;
            case State.Hit:

                break;
            case State.NotHit:
                transform.position = Vector3.Lerp(transform.position, targetPos, Time.deltaTime * flySpeed);//move towards hit position

                if (!PlayingCough && Vector3.Distance(transform.position, targetPos) < 0.1f)
                {
                    GetComponent<AudioSource>().PlayOneShot(CoughSFX[Random.Range(0, CoughSFX.Length)]);
                    Destroy(this.gameObject, 1);
                    PlayingCough = true;
                }
                break;
        }
    }

    public void SetBeat(Beat nextBeat)
    {
        beat = nextBeat;
    }

    public void SetDanceArea(Transform bottomLeft, Transform bottomRight, Transform topLeft, Transform topRight)
    {
        this.danceBottomLeft = bottomLeft;
        this.danceBottomRight = bottomRight;
        this.danceTopLeft = topLeft;
        this.danceTopRight = topRight;
    }

    public void SetTargetArea(Transform bottomLeft, Transform bottomRight, Transform topLeft, Transform topRight)
    {
        this.bottomLeft = bottomLeft;
        this.bottomRight = bottomRight;
        this.topLeft = topLeft;
        this.topRight = topRight;
    }

    void OnTriggerStay(Collider otherCollider)
    {
        if (otherCollider.CompareTag("Player"))
        {
            if (TrackManager.Instance.GetActiveTrackTimeInMs() < beat.Time + gestureTimeWindow && TrackManager.Instance.GetActiveTrackTimeInMs() > beat.Time - gestureTimeWindow)
            {
                flySwatted = true;
            }
        }
    }


    private Vector3 calculateRandomPos(Vector3 botL, Vector3 topL, Vector3 topR, Vector3 botR)
    {
        //calculate random point to fly towards
        float weight1 = Random.Range(0.0f, 1.0f);
        float weight2 = Random.Range(0.0f, 1.0f);
        float weight3 = Random.Range(0.0f, 1.0f);
        float weight4 = Random.Range(0.0f, 1.0f);

        float sum = weight1 + weight2 + weight3 + weight4;

        weight1 /= sum;
        weight2 /= sum;
        weight3 /= sum;
        weight4 /= sum;

        Vector3 result = weight1 * topL + weight2 * topR + weight3 * botL + weight4 * botR;
        return result;
    }

    IEnumerator RandomDanceRoutine(float timeToDance, int tickCount)
    {
        danceStartTime = TrackManager.Instance.GetActiveTrackTimeInMs();

        tickInterval = timeToDance / tickCount;

        for (int i = 0; i < tickCount; i++)
        {
            currentTick = i;

            startPos = transform.position;
            targetPos = calculateRandomPos(danceBottomLeft.position, danceTopLeft.position, danceTopRight.position, danceBottomRight.position);
            yield return new WaitForSeconds(tickInterval * 0.001f);
        }

    }
}
