﻿using UnityEngine;
using System.Collections;
using Windows.Kinect;

public class KomischerGuyController : BasicAvatarController 
{
    public override void Start()
    {
        SpineBase = GameObject.Find("joint_HipMaster").transform;
        //SpineMid = GameObject.Find("Character1_Spine2").transform;
        Neck = GameObject.Find("joint_Neck").transform;
        Head = GameObject.Find("joint_Head").transform;
        ShoulderLeft = GameObject.Find("joint_ShoulderLT").transform;
        ElbowLeft = GameObject.Find("joint_ElbowLT").transform;
        WristLeft = GameObject.Find("joint_HandLT").transform;
        //HandLeft = GameObject.Find("").transform;
        ShoulderRight = GameObject.Find("joint_ShoulderRT").transform;
        ElbowRight = GameObject.Find("joint_ElbowRT").transform;
        WristRight = GameObject.Find("joint_HandRT").transform;
        //HandRight = GameObject.Find("").transform;
        HipLeft = GameObject.Find("joint_HipLT").transform;
        KneeLeft = GameObject.Find("joint_KneeLT").transform;
        AnkleLeft = GameObject.Find("joint_FootLT").transform;
        FootLeft = GameObject.Find("joint_ToeLT").transform;

        HipRight = GameObject.Find("joint_HipRT").transform;
        KneeRight = GameObject.Find("joint_KneeRT").transform;
        AnkleRight = GameObject.Find("joint_FootRT").transform;
        FootRight = GameObject.Find("joint_ToeRT").transform;

        //SpineShoulder = GameObject.Find("Character1_Spine2").transform;


        base.Start();
    }


    public virtual void Update()
    {
        base.Update();
    }

}
