﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ColorImageKinect : MonoBehaviour {

    public CoordinateMapperManager _CoordinateMapperManager;

	// Use this for initialization
	void Start () {
        Texture2D kinectColorTex = _CoordinateMapperManager.GetColorTexture();
        if (kinectColorTex != null)
        {
            GetComponent<RawImage>().texture = kinectColorTex;
            GetComponent<RawImage>().material.SetTexture("_MainTex", kinectColorTex);
        }
	}
}
