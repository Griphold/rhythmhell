using UnityEngine;
using System.Collections;
using Windows.Kinect;
using System.Runtime.InteropServices;
using System;

public class CoordinateMapperManager : MonoBehaviour
{
    private KinectSensor m_pKinectSensor;
    private CoordinateMapper m_pCoordinateMapper;
    private MultiSourceFrameReader m_pMultiSourceFrameReader;
    private DepthSpacePoint[] m_pDepthCoordinates;

    private byte[] pColorBuffer;
    const int cColorWidth = 1920;
    const int cColorHeight = 1080;

    long frameCount = 0;

    double elapsedCounter = 0.0;
    double fps = 0.0;

    Texture2D m_pColorRGBX;

    bool nullFrame = false;

    void Awake()
    {
        pColorBuffer = new byte[cColorWidth * cColorHeight * 4];

        m_pColorRGBX = new Texture2D(cColorWidth, cColorHeight, TextureFormat.RGBA32, false);

        m_pDepthCoordinates = new DepthSpacePoint[cColorWidth * cColorHeight];

        InitializeDefaultSensor();
    }

    public Texture2D GetColorTexture()
    {
        return m_pColorRGBX;
    }

    void InitializeDefaultSensor()
    {
        m_pKinectSensor = KinectSensor.GetDefault();

        if (m_pKinectSensor != null)
        {
            // Initialize the Kinect and get coordinate mapper and the frame reader
            m_pCoordinateMapper = m_pKinectSensor.CoordinateMapper;

            m_pKinectSensor.Open();
            if (m_pKinectSensor.IsOpen)
            {
                m_pMultiSourceFrameReader = m_pKinectSensor.OpenMultiSourceFrameReader(
                    FrameSourceTypes.Color);
            }
        }

        if (m_pKinectSensor == null)
        {
            UnityEngine.Debug.LogError("No ready Kinect found!");
        }
    }

    void ProcessFrame()
    {
        m_pColorRGBX.LoadRawTextureData(pColorBuffer);
        m_pColorRGBX.Apply();
    }


    void Update()
    {

        if (m_pMultiSourceFrameReader == null)
        {
            return;
        }

        var pMultiSourceFrame = m_pMultiSourceFrameReader.AcquireLatestFrame();
        if (pMultiSourceFrame != null)
        {
            frameCount++;
            nullFrame = false;

            using (var pColorFrame = pMultiSourceFrame.ColorFrameReference.AcquireFrame())
            {
                // Get Color Frame Data
                if (pColorFrame != null)
                {
                    var pColorData = GCHandle.Alloc(pColorBuffer, GCHandleType.Pinned);
                    pColorFrame.CopyConvertedFrameDataToIntPtr(pColorData.AddrOfPinnedObject(), (uint)pColorBuffer.Length, ColorImageFormat.Rgba);
                    pColorData.Free();
                }

            }

            ProcessFrame();
        }
        else
        {
            nullFrame = true;
        }
    }

    void OnApplicationQuit()
    {
        pColorBuffer = null;

        if (m_pMultiSourceFrameReader != null)
        {
            m_pMultiSourceFrameReader.Dispose();
            m_pMultiSourceFrameReader = null;
        }

        if (m_pKinectSensor != null)
        {
            m_pKinectSensor.Close();
            m_pKinectSensor = null;
        }
    }
}

