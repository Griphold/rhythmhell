﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;

public class MenuHandler : MonoBehaviour
{

    public Canvas StartMenu;
    public Canvas MainMenu;
    public Canvas OptionMenu;
    public Slider VolumeSlider;

    void Start()
    {
        StartMenu = StartMenu.GetComponent<Canvas>();
        MainMenu = MainMenu.GetComponent<Canvas>();
        OptionMenu = OptionMenu.GetComponent<Canvas>();
        StartMenu.enabled = false;
        OptionMenu.enabled = false;
        VolumeSlider.value = TrackManager.Volume;
    }

    public void StartGame()
    {
        StartMenu.enabled = true;
        MainMenu.enabled = false;
    }

    public void Option()
    {
        OptionMenu.enabled = true;
        MainMenu.enabled = false;
    }

    public void OptionVolume()
    {
        float volume = VolumeSlider.value;
        TrackManager.Volume = volume;
    }

    public void QuitGame()
    {
        Application.Quit();
    }

    public void BackToMainMenu()
    {
        StartMenu.enabled = false;
        OptionMenu.enabled = false;
        MainMenu.enabled = true;
    }

    public void StartFly()
    {
       SceneManager.LoadScene("FlySwatter");
    }

    public void StartHoot()
    {
        SceneManager.LoadScene("HootHarmony");
    }

    public void StartToddler()
    {
        SceneManager.LoadScene("ToddlerTantrum");
    }
}
