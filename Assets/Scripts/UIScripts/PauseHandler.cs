﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PauseHandler : MonoBehaviour
{

    public Canvas PauseMenu;
    public Canvas PauseButtonCanvas;
    public Canvas OptionCanvas;
    public Slider VolumeSlider;

    // Use this for initialization
    void Start()
    {
        PauseMenu = PauseMenu.GetComponent<Canvas>();
        PauseButtonCanvas = PauseButtonCanvas.GetComponent<Canvas>();
        OptionCanvas = OptionCanvas.GetComponent<Canvas>();
        PauseMenu.enabled = false;
        OptionCanvas.enabled = false;
        VolumeSlider.value = TrackManager.Volume;
    }


    public void PauseButtonFunction()
    {
        PauseMenu.enabled = true;
        PauseButtonCanvas.enabled = false;
        Time.timeScale = 0.0f;
        TrackManager.Instance.SetPaused(true);
    }

    public void BackToGameButton()
    {
        PauseMenu.enabled = false;
        PauseButtonCanvas.enabled = true;
        Time.timeScale = 1.0f;
        TrackManager.Instance.SetPaused(false);
    }
    public void RestartButton()
    {
        Time.timeScale = 1.0f;
        SceneManager.LoadScene(SceneManager.GetActiveScene().name, LoadSceneMode.Single);
    }

    public void OptionButton()
    {
        PauseMenu.enabled = false;
        OptionCanvas.enabled = true;
    }

    public void OptionBackButton()
    {
        OptionCanvas.enabled = false;
        PauseMenu.enabled = true;
    }

    public void OptionVolume()
    {
        float volume = VolumeSlider.value;
        TrackManager.Instance.SetVolume(volume);
    }

    public void QuitToMainMenu()
    {
        Time.timeScale = 1f;
        SceneManager.LoadScene("MainMenu");
    }
}
