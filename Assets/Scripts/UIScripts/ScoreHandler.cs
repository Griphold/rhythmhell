﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ScoreHandler : MonoBehaviour {

    public Canvas ScoreCanvas;
    public Canvas PauseButtonCanvas;
    public Text ScoreText;
    public Gradient ScoreColor;

    private float currentPoints;
	// Use this for initialization
	void Start () {

        ScoreCanvas = ScoreCanvas.GetComponent<Canvas>();
        PauseButtonCanvas = PauseButtonCanvas.GetComponent<Canvas>();
        ScoreCanvas.enabled = false;
	    
    }

    void Update()
    {
        if (TrackManager.Instance.IsActiveTrackOver())
        {
            ScoreCanvas.enabled = true;
            PauseButtonCanvas.enabled = false;
        }
    }

    public void RestartButton()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }
    public void QuitToMenu()
    {
        SceneManager.LoadScene("MainMenu");
    }

    public void SetScore(float score)
    {
        ScoreText.color = Color.Lerp(Color.red, Color.green, score);
        score = Mathf.RoundToInt(score * 10000);
        ScoreText.text = "" + (score/100) + "%";
    }


}
