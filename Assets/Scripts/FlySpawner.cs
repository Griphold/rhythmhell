﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class FlySpawner : MonoBehaviour 
{
    public GameObject FlyPrefab;
    public Transform FlySpawnPoint;

    public Transform bottomLeft, bottomRight, topLeft, topRight;//corners of the rectangle to fly towards
    public Transform danceBottomLeft, danceBottomRight, danceTopLeft, danceTopRight;

    private List<Beat> processedBeats = new List<Beat>();	

	// Update is called once per frame
	void Update () 
    {
        Beat nextBeat = TrackManager.Instance.GetNextBeat();
        float currentTrackTime = TrackManager.Instance.GetActiveTrackTimeInMs();

        if (nextBeat != null)
        {
            if (nextBeat.Time - currentTrackTime < 6000 && !processedBeats.Contains(nextBeat))
            {
                
                SpawnFly(nextBeat);
                processedBeats.Add(nextBeat);
                
            }
        }
	}

    private void SpawnFly(Beat beat)
    {
        GameObject flyObject = (GameObject) GameObject.Instantiate(FlyPrefab, GetSpawnPosition(), Quaternion.identity);
        Fly f = flyObject.GetComponent<Fly>();

        f.SetBeat(beat);

        f.SetDanceArea(danceBottomLeft, danceBottomRight, danceTopLeft, danceTopRight);
        f.SetTargetArea(bottomLeft, bottomRight, topLeft, topRight);
    }

    private Vector3 GetSpawnPosition()
    {
        return FlySpawnPoint.position;
    }
}
