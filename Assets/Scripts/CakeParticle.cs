﻿using UnityEngine;
using System.Collections;

public class CakeParticle : MonoBehaviour 
{
    private ParticleSystem.CollisionModule partCol;

	// Use this for initialization
	void Start () 
    {
        ParticleSystem partSys = GetComponent<ParticleSystem>();
        partCol = partSys.collision;

        GameObject particleCollisionPlane = GameObject.FindGameObjectWithTag("particleCollisionPlane");
        partCol.SetPlane(0, particleCollisionPlane.transform);
	}
}
