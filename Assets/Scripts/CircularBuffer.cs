﻿using UnityEngine;
using System.Collections;

public class CircularBuffer {
    private Vector3[] buffer;
    private int size;
    private int index = 0;

    public CircularBuffer(int Size)
    {
        buffer = new Vector3[Size];
        size = Size;
    }

    public void Push(Vector3 vector)
    {
        buffer[index] = vector;
        index = (index + 1) % size;
    }

    public Vector3 GetAverage()
    {
        Vector3 result = Vector3.zero;

        foreach (Vector3 v in buffer)
        {
            result += v;
        }

        return result / size;
    }
}
