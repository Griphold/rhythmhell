﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class OwlController : MonoBehaviour
{
    public Transform Eye_Pupil_Left;
    public Transform Eye_Pupil_Right;
    public Transform Eye_Lid_Left;
    public Transform Eye_Lid_Right;
    public bool LeftOfPlayer;

    public AudioClip[] FailSounds;


    private Animator owlAnimator;
    private Vector3 startingScale;
    private List<Beat> processedBeats = new List<Beat>();

    // Use this for initialization
    void Start()
    {
        startingScale = transform.localScale;
        owlAnimator = GetComponent<Animator>();
        TrackManager.Instance.BeatOverEvent += OnBeatOver;
    }

    // Update is called once per frame
    void Update()
    {
        TrackManager tm = TrackManager.Instance;
        Beat nextBeat = tm.GetNextBeat();
        float currentTimeInSong = tm.GetActiveTrackTimeInMs();
        float timeWindow = tm.GetActiveTrack().GestureTimeWindow;

        if (tm.HasStarted() && nextBeat != null && !processedBeats.Contains(nextBeat))
        {
            float delay = (nextBeat.Time - currentTimeInSong) / 1000;

            if (nextBeat.type == Beat.Type.Basic)
            {
                StartCoroutine(startSquishAnimation(delay));
            }
            else if (nextBeat.type == Beat.Type.Flap)
            {
                delay -= 0.6f;
                StartCoroutine(startFlapAnimation(delay));
            }

            processedBeats.Add(nextBeat);
        }

        if (EyesTimer > 0)
        {
            EyesTimer -= Time.deltaTime;
        }
        else
        {
            SetNormalEyes();
        }


        /*Beat lastBeat = tm.GetLastBeat();
        float trackStartInMs = tm.GetActiveTrackStartInMs();
        float currentTimeInSong = tm.GetActiveTrackTimeInMs();
        float trackLength = tm.GetActiveTrackLengthInMs();

        float timeToLastBeat = lastBeat == null ? currentTimeInSong : Mathf.Abs(currentTimeInSong - lastBeat.Time);
        float timeToNextBeat = nextBeat == null ? trackLength - currentTimeInSong : Mathf.Abs(nextBeat.Time - currentTimeInSong);

        float totalTime = timeToLastBeat + timeToNextBeat;
        float value = timeToLastBeat / totalTime;
        if (lastBeat == null)
        {
            value = Mathf.Clamp(value, 0.5f, 1);
        }
        if (nextBeat == null)
        {
            value = Mathf.Clamp(value, 0, 0.5f);
        }

        float scalingFactor = ScalingFactorBetweenBeats.Evaluate(value);

        transform.localScale = new Vector3(startingScale.x * (2 - scalingFactor), startingScale.y * scalingFactor, 1);*/

    }

    private float EyesTimer = 0;
    //when a beat is failed
    public void OnBeatOver(object sender, BeatEventArgs e)
    {
        //Debug.Log("Bad Beat m8");

        SetDissapoitedEyes();

        EyesTimer = 1f;
    }

    private void SetDissapoitedEyes()
    {
        if (EyesTimer <= 0)
        {
            //play sound effect
            GetComponent<AudioSource>().PlayOneShot(FailSounds[Random.Range(0, FailSounds.Length)]);
        }

        Eye_Lid_Left.GetComponent<Renderer>().enabled = true;
        Eye_Lid_Right.GetComponent<Renderer>().enabled = true;

        if (LeftOfPlayer)
        {
            Eye_Pupil_Left.localPosition = new Vector3(-0.08f, 0, 0);
            Eye_Pupil_Right.localPosition = new Vector3(-0.08f, 0, 0);
        }
        else
        {
            Eye_Pupil_Left.localPosition = new Vector3(0.08f, 0, 0);
            Eye_Pupil_Right.localPosition = new Vector3(0.08f, 0, 0);
        }
    }

    private void SetNormalEyes()
    {
        Eye_Lid_Left.GetComponent<Renderer>().enabled = false;
        Eye_Lid_Right.GetComponent<Renderer>().enabled = false;

        Eye_Pupil_Left.localPosition = Vector3.zero;
        Eye_Pupil_Right.localPosition = Vector3.zero;
    }

    private IEnumerator startSquishAnimation(float delay)
    {
        yield return new WaitForSeconds(delay);

        owlAnimator.SetBool("beat", true);

        yield return null;

        owlAnimator.SetBool("beat", false); ;
    }

    private IEnumerator startFlapAnimation(float delay)
    {
        yield return new WaitForSeconds(delay);

        owlAnimator.SetBool("flap", true);

        yield return null;

        owlAnimator.SetBool("flap", false); ;
    }
}
