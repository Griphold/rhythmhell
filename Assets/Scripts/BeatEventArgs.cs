﻿using UnityEngine;
using System;

public class BeatEventArgs : EventArgs
{

    public BeatEventArgs(Beat b)
    {
        this.beat = b;
    }
    public Beat beat { get; set; }
}
