﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class AdjustMenuVolume : MonoBehaviour {

    public AudioSource audioSource;
    public Slider volumeslider;

	// Use this for initialization
    void Update()
    {
        audioSource.volume = volumeslider.value;
    }
}
