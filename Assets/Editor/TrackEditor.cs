﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;

[CustomEditor(typeof(TrackManager))]
public class TrackEditor: Editor {

    private List<int> beatOffsets = new List<int>();
    private List<List<Gesture.Type>> beatGestures = new List<List<Gesture.Type>>(); 


    public override void OnInspectorGUI()
    {
        TrackManager trackManager = (TrackManager) this.target;
        
        DrawDefaultInspector();

        //create a list of beat offsets for every track
        while (beatOffsets.Count < trackManager.TrackList.Count)//ensure beat list is not smaller
        {
            beatOffsets.Add(0);
        }

        //create a list of gesture arrays for every track
        while (beatGestures.Count < trackManager.TrackList.Count)//ensure gesture array list is not smaller
        {
            List<Gesture.Type> gestures = new List<Gesture.Type>(3);
            gestures.Add(Gesture.Type.None);
            gestures.Add(Gesture.Type.None);
            gestures.Add(Gesture.Type.None);

            beatGestures.Add(gestures);

        }

        for (int i = 0; i < trackManager.TrackList.Count; i++)
        {
            EditorGUILayout.BeginHorizontal();

            EditorGUILayout.PrefixLabel("Track " + i );
            beatOffsets[i] = EditorGUILayout.IntField(beatOffsets[i]);//create an int field for every track         

            //if beat offset is applied, change all beat times
            if (GUILayout.Button("Apply"))
            {
                Undo.RecordObject(target, "Applied Beat Offset " + beatOffsets[i] + " to Track " + i);//enable undo

                foreach(Beat b in trackManager.TrackList[i].Beats)
                {
                    b.Time = b.Time + beatOffsets[i];
                }

                Debug.Log("Applied Beat Offset " + beatOffsets[i] + " to Track " + i);
                EditorUtility.SetDirty(target);//signal change
            }

            EditorGUILayout.EndHorizontal();



            EditorGUILayout.BeginHorizontal();

            beatGestures[i][0] = (Gesture.Type)EditorGUILayout.EnumPopup(beatGestures[i][0]);
            beatGestures[i][1] = (Gesture.Type)EditorGUILayout.EnumPopup(beatGestures[i][1]);
            beatGestures[i][2] = (Gesture.Type)EditorGUILayout.EnumPopup(beatGestures[i][2]);


            //if beat gestures are applied, change all beat gestures
            if (GUILayout.Button("Apply"))
            {
                Undo.RecordObject(target, "Applied Beat Gesture change to Track " + i);//enable undo

                foreach (Beat b in trackManager.TrackList[i].Beats)
                {
                    List<Gesture.Type> notNone = beatGestures[i].FindAll(x => { return x != Gesture.Type.None; });
                    b.AcceptedGestures = notNone;
                }

                Debug.Log("Applied Beat Gesture change to Track " + i);
                EditorUtility.SetDirty(target);
            }

            EditorGUILayout.EndHorizontal();
        }

    }
}
